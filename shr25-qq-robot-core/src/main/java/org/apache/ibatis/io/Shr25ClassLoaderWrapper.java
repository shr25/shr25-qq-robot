package org.apache.ibatis.io;

import com.shr25.robot.classLoader.Shr25PluginClassLoad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Shr25ClassLoaderWrapper extends ClassLoaderWrapper {

    private static Map<String, Shr25PluginClassLoad> shr25PluginClassLoads = new HashMap<>();

    public static void addShr25PluginClassLoad(Shr25PluginClassLoad shr25PluginClassLoad){
        if(!shr25PluginClassLoads.containsKey(shr25PluginClassLoad.getPathName())){
            shr25PluginClassLoads.put(shr25PluginClassLoad.getPathName(), shr25PluginClassLoad);
        }
    }

    /**
     * 插件是否加载
     * @param shr25PluginPath 插件路径
     */
    public static Shr25PluginClassLoad getShr25PluginClassLoader(String shr25PluginPath){
        return shr25PluginClassLoads.get(shr25PluginPath);
    }

    /**
     * 插件是否加载
     * @param shr25PluginPath 插件路径
     */
    public static boolean hasShr25PluginClassLoader(String shr25PluginPath){
        return shr25PluginClassLoads.containsKey(shr25PluginPath);
    }

    public static void removeShr25PluginClassLoad(Shr25PluginClassLoad shr25PluginClassLoad){
        shr25PluginClassLoads.remove(shr25PluginClassLoad.getPathName());
    }

    @Override
    ClassLoader[] getClassLoaders(ClassLoader classLoader) {
        ClassLoader[] oldClassLoaders =  super.getClassLoaders(classLoader);


        ClassLoader[] classLoaders = new ClassLoader[shr25PluginClassLoads.size() + oldClassLoaders.length];
        Integer count = 0;
        for (Map.Entry<String, Shr25PluginClassLoad> entry: shr25PluginClassLoads.entrySet()) {
            classLoaders[count] = entry.getValue();
            count++;
        }

        for (int i = 0; i < oldClassLoaders.length; i++) {
            classLoaders[i+count] = oldClassLoaders[i];
        }

        return classLoaders;
    }
}
