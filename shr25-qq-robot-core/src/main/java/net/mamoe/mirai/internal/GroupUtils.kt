package net.mamoe.mirai.internal

import net.mamoe.mirai.Bot

object GroupUtils{
    @JvmStatic
    @Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
    fun getGetTroopListSimplify(bot: Bot): Any{
        val kFunction = net.mamoe.mirai.internal.network.protocol.packet.list.FriendList.GetTroopListSimplify::invoke;
        val clientMp = net.mamoe.mirai.internal.QQAndroidBot::client;
        val outgoingPacketWithRespType = kFunction.call(clientMp.get(bot as net.mamoe.mirai.internal.QQAndroidBot));
        return outgoingPacketWithRespType;
    }
    @JvmStatic
    @Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")
    fun getNetwork(bot: Bot): Any{
        val clientMp = net.mamoe.mirai.internal.QQAndroidBot::network;
        return clientMp.get(bot as net.mamoe.mirai.internal.QQAndroidBot);
    }
}
