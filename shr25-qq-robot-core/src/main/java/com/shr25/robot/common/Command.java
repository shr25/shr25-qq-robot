package com.shr25.robot.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 指令
 */
@Getter
@Setter
public class Command {
    /**
     * 指令
     */
    private String commandStr;

    /**
     * 描述
     */
    private String desc;

    /**
     * 最小权限
     */
    private RobotMsgPermission permission = RobotMsgPermission.ADMINISTRATOR;

    /**
     * 默认接受消息
     */
    private RobotMsgType[] robotMsgTypes = {RobotMsgType.GroupAtBot, RobotMsgType.Group, RobotMsgType.Friend};

    /**
     * 消息处理器
     */
    private ExecuteMessage executeMessage;

    /**
     * 排序
     */
    private Integer srot;

    public Command(Integer srot, String commandStr, String desc, ExecuteMessage executeMessage) {
        this.srot = srot;
        this.commandStr = commandStr;
        this.desc = desc;
        this.executeMessage = executeMessage;
    }

    public Command(Integer srot, String commandStr, String desc, RobotMsgPermission permission, ExecuteMessage executeMessage) {
        this.srot = srot;
        this.commandStr = commandStr;
        this.desc = desc;
        this.permission = permission;
        this.executeMessage = executeMessage;
    }

    public Command(Integer srot, String commandStr, String desc, RobotMsgType[] robotMsgTypes, ExecuteMessage executeMessage) {
        this.srot = srot;
        this.commandStr = commandStr;
        this.desc = desc;
        this.robotMsgTypes = robotMsgTypes;
        this.executeMessage = executeMessage;
    }

    public Command(Integer srot, String commandStr, String desc, RobotMsgPermission permission, RobotMsgType[] robotMsgTypes, ExecuteMessage executeMessage) {
        this.srot = srot;
        this.commandStr = commandStr;
        this.desc = desc;
        this.permission = permission;
        this.robotMsgTypes = robotMsgTypes;
        this.executeMessage = executeMessage;
    }
}
