package com.shr25.robot.utils;

import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class ManifestUtils {

    /**
     * 只有在查找具体某个类所在jar包的Manifest信息时, 才会加载对应Manifest
     * 优化: 可以在找到后做缓存
     */
    public static Manifest getJarManifest(Class<?> clazz) {
        ArrayList<URL> resourceUrls = new ArrayList<>();

        ClassLoader classLoader = clazz.getClassLoader();
        String sourceName = clazz.getName().replace('.', '/').concat(".class");
        try {
            Enumeration<URL> resources = classLoader.getResources(sourceName);
            while (resources.hasMoreElements()) {
                resourceUrls.add(resources.nextElement());
            }
            if (resourceUrls.size() > 1) {
                //log.warn("class:{}在多个不同的包中:{}", clazz.getName(), resourceUrls);
            }
            if (resourceUrls.size() > 0) {
                URL url = resourceUrls.get(0);
                URLConnection urlConnection = url.openConnection();
                if (urlConnection instanceof JarURLConnection) {
                    JarURLConnection jarURL = (JarURLConnection) urlConnection;
                    JarFile jarFile = jarURL.getJarFile();
                    Manifest manifest = jarFile.getManifest();
                    jarFile.close();
                    return manifest;
                } else {
                    //TODO 需要对非jar做处理
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
