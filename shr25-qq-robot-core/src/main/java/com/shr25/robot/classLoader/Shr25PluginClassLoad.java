package com.shr25.robot.classLoader;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.JarClassLoader;
import cn.hutool.core.lang.SimpleCache;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.shr25.robot.qq.plugins.RobotPlugin;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.binding.MapperProxyFactory;
import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.io.Shr25ClassLoaderWrapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@Slf4j
public class Shr25PluginClassLoad extends JarClassLoader {

    /**
     * 插件路径
     */
    private String pathName;

    /**
     * 销毁时是否删除jar
     */
    private Boolean isDeleteJar = false;

    @Nullable
    private Class<? extends Annotation> initAnnotationType;

    @Nullable
    private Class<? extends Annotation> destroyAnnotationType;

    private final transient LifecycleMetadata emptyLifecycleMetadata =
            new LifecycleMetadata(Object.class, Collections.emptyList(), Collections.emptyList()) {
                @Override
                public void checkConfigMembers(RootBeanDefinition beanDefinition) {
                }
                @Override
                public void invokeInitMethods(Object target, String beanName) {
                }
                @Override
                public void invokeDestroyMethods(Object target, String beanName) {
                }
                @Override
                public boolean hasDestroyMethods() {
                    return false;
                }
            };

    @Nullable
    private final transient Map<Class<?>, LifecycleMetadata> lifecycleMetadataCache = new ConcurrentHashMap<>(256);

    /**
     * bean名称列表
     * */
    private List<String> beanNames = new ArrayList<>();

    /**
     * mapper列表
     */
    private List<Class> mappers = new ArrayList<>();

    /**
     * 插件列表
     */
    private List<RobotPlugin> robotPlugins = new ArrayList<>();

    public Shr25PluginClassLoad(File fileOrDir) throws Exception {
        super();
        pathName = fileOrDir.getAbsolutePath();
        addJar(fileOrDir);
        setInitAnnotationType(PostConstruct.class);
        setDestroyAnnotationType(PreDestroy.class);
        loadJar();
    }

    public String getPathName() {
        return pathName;
    }

    @Nullable
    public Class<? extends Annotation> getInitAnnotationType() {
        return initAnnotationType;
    }

    public void setInitAnnotationType(@Nullable Class<? extends Annotation> initAnnotationType) {
        this.initAnnotationType = initAnnotationType;
    }

    @Nullable
    public Class<? extends Annotation> getDestroyAnnotationType() {
        return destroyAnnotationType;
    }

    public void setDestroyAnnotationType(@Nullable Class<? extends Annotation> destroyAnnotationType) {
        this.destroyAnnotationType = destroyAnnotationType;
    }

    public List<RobotPlugin> getRobotPlugins() {
        return robotPlugins;
    }

    /**
     * 加载jar包中的类，并注册到springBean容器中
     */
    public void loadJar() throws Exception {
        log.info("开始加载插件：{}", pathName);
        //mapper列表
        List<Class> mapperTypes = new ArrayList<>();
        //bean列表
        List<Class> beanTypes = new ArrayList<>();
        try {
            for (URL url : getURLs()) {
                URL jarUrl =  new URL("jar:" + url.toURI() + "!/");
                JarURLConnection conn = (JarURLConnection) jarUrl.openConnection();
                JarFile jarFile = conn.getJarFile(); //获取jarFile
                //解析jar包每一项
                Enumeration<JarEntry> en = jarFile.entries();
                while (en.hasMoreElements()) {
                    JarEntry je = en.nextElement();
                    String name = je.getName();
                    //这里添加了路径扫描限制
                    if (name.endsWith(".class")) {
                        String className = name.replace(".class", "").replaceAll("/", ".");
                        Class<?> clazz = null;
                        if(jarFile.getName().indexOf("protocol-version-plugin")> -1){
                            if(className.startsWith("com.shr25")) {
                                clazz = loadClass(className);
                            }
                        }else{
                            clazz = loadClass(className);
                        }
                        if(clazz != null){
                            if (Mapper.class.isAssignableFrom(clazz)) {
                                mapperTypes.add(clazz);
                            } else {
                                Component component = AnnotationUtils.findAnnotation(clazz, Component.class);
                                if (component != null) {
                                    beanTypes.add(clazz);
                                }
                            }
                        }
                    }
                }

            }

            registerMapperBean(mapperTypes);
            registerBean(beanTypes);

            postProcessBeforeInitialization();
        }catch (Exception e){
            log.error("加载插件失败，开始卸载插件");
            unLoadJar(false);
           throw e;
        }
    }

    /**
     * 从SpringBean容器中移出已经加载了jar包中的类
     * @param isDeleteJar 是否删除
     */
    public void unLoadJar(boolean isDeleteJar){
        log.info("开始卸载jar:{}", pathName);
        robotPlugins.clear();
        log.info("开始销毁bean");
        for (String beanNmae: beanNames) {
            log.info("开始执行bean--{}--销毁方法", beanNmae);
            postProcessBeforeDestruction(SpringUtil.getBean(beanNmae), beanNmae);
            log.info("执行bean--{}--销毁方法完毕", beanNmae);
            log.info("spring开始注销bean--{}", beanNmae);
            SpringUtil.unregisterBean(beanNmae);
            log.info("spring注销bean完毕--{}", beanNmae);
        }
        beanNames.clear();
        MybatisConfiguration mybatisConfiguration = SpringUtil.getBean(MybatisConfiguration.class);
        MapperRegistry mapperRegistry =  mybatisConfiguration.getMapperRegistry();
        Map<Class<?>, MapperProxyFactory<?>> knownMappers = (Map<Class<?>, MapperProxyFactory<?>>) ReflectUtil.getFieldValue(mapperRegistry, "knownMappers");
        log.info("开始销毁mapper");
        for (Class clazz: mappers) {
            log.info("开始注销mapper--{}", clazz.getName());
            knownMappers.remove(clazz);
            log.info("注销mapper完毕--{}", clazz.getName());
        }
        mappers.clear();
        log.info("销毁mapper完毕");


        log.info("开始清除hutool反射缓存");
        try {
            Field methodsCache = ReflectUtil.class.getDeclaredField("METHODS_CACHE");
            methodsCache.setAccessible(true);
            SimpleCache<Class<?>, Method[]> METHODS_CACHE = (SimpleCache<Class<?>, Method[]>) methodsCache.get(null);
            METHODS_CACHE.clear();

            Field constructorsCache = ReflectUtil.class.getDeclaredField("CONSTRUCTORS_CACHE");
            constructorsCache.setAccessible(true);
            SimpleCache<Class<?>, Constructor<?>[]> CONSTRUCTORS_CACHE = (SimpleCache<Class<?>, Constructor<?>[]>) constructorsCache.get(null);
            CONSTRUCTORS_CACHE.clear();
        }catch (Exception e){
            log.info("清除hutool反射缓存失败", e);
        }
        log.info("清除hutool反射缓存完毕");

        try {
            log.info("开始关闭classLoader");
            close();
            log.info("关闭classLoader完毕");
        } catch (IOException e) {
            log.info("关闭classLoader异常", e);
            throw new RuntimeException(e);
        }
        log.info("卸载jar完毕:{}", pathName);
        this.isDeleteJar = isDeleteJar;

        log.info("开始移除classLoader:{}", pathName);
        Shr25ClassLoaderWrapper.removeShr25PluginClassLoad(this);
        log.info("移除classLoader完毕:{}", pathName);

        System.gc();
    }

    /**
     * bean直接可以有依赖关系，采用循环注入的方式
     * @param mapperTypes
     * @throws Exception
     */
    private void registerMapperBean(List<Class> mapperTypes) throws Exception {
        MybatisConfiguration mybatisConfiguration = SpringUtil.getBean(MybatisConfiguration.class);
        MapperRegistry mapperRegistry =  mybatisConfiguration.getMapperRegistry();
        SqlSessionTemplate sqlSessionTemplate = SpringUtil.getBean("sqlSessionTemplate");
        for (Class clazz: mapperTypes) {
            mapperRegistry.addMapper(clazz);
            mappers.add(clazz);
            String beanName = StrUtil.lowerFirst(clazz.getSimpleName());
            SpringUtil.registerBean(beanName, mapperRegistry.getMapper(clazz, sqlSessionTemplate));
            beanNames.add(beanName);
        }
    }

    /**
     * bean直接可以有依赖关系，采用循环注入的方式
     * @param beanTypes
     * @throws Exception
     */
    private void registerBean(List<Class> beanTypes) throws Exception {
        List<Class> beanTypeList = new ArrayList<>();
        Exception err = null;
        for (Class clazz: beanTypes) {
            try{
                String beanName = StrUtil.lowerFirst(clazz.getSimpleName());
                SpringUtil.registerBean(beanName, clazz.newInstance());
                beanNames.add(beanName);
                if(RobotPlugin.class.isAssignableFrom(clazz)){
                    RobotPlugin robotPlugin = SpringUtil.getBean(beanName);
                    robotPlugins.add(robotPlugin);
                }
            }catch (Exception e){
                err = e;
                beanTypeList.add(clazz);
            }
        }
        if(beanTypeList.size() == beanTypes.size()){
            throw new Exception("无法加载插件:" + getURLs()[0].getPath(), err);
        }
        if(beanTypeList.size() > 0){
            registerBean(beanTypeList);
        }
    }

    private void postProcessBeforeInitialization() {
        Shr25ClassLoaderWrapper.addShr25PluginClassLoad(this);
        for (String beanName : beanNames) {
            postProcessBeforeInitialization(SpringUtil.getBean(beanName), beanName);
        }
    }

    /**
     * 执行初始化方法
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        LifecycleMetadata metadata = findLifecycleMetadata(bean.getClass());
        try {
            metadata.invokeInitMethods(bean, beanName);
        }
        catch (InvocationTargetException ex) {
            throw new BeanCreationException(beanName, "Invocation of init method failed", ex.getTargetException());
        }
        catch (Throwable ex) {
            throw new BeanCreationException(beanName, "Failed to invoke init method", ex);
        }
        return bean;
    }

    /**
     * 执行销毁方法
     * @param bean
     * @param beanName
     * @throws BeansException
     */
    public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
        LifecycleMetadata metadata = findLifecycleMetadata(bean.getClass());
        try {
            metadata.invokeDestroyMethods(bean, beanName);
        }
        catch (InvocationTargetException ex) {
            String msg = "Destroy method on bean with name '" + beanName + "' threw an exception";
            if (log.isDebugEnabled()) {
                log.warn(msg, ex.getTargetException());
            }
            else {
                log.warn(msg + ": " + ex.getTargetException());
            }
        }
        catch (Throwable ex) {
            log.warn("Failed to invoke destroy method on bean with name '" + beanName + "'", ex);
        }
    }


    private LifecycleMetadata findLifecycleMetadata(Class<?> clazz) {
        if (this.lifecycleMetadataCache == null) {
            // Happens after deserialization, during destruction...
            return buildLifecycleMetadata(clazz);
        }
        // Quick check on the concurrent map first, with minimal locking.
        LifecycleMetadata metadata = this.lifecycleMetadataCache.get(clazz);
        if (metadata == null) {
            synchronized (this.lifecycleMetadataCache) {
                metadata = this.lifecycleMetadataCache.get(clazz);
                if (metadata == null) {
                    metadata = buildLifecycleMetadata(clazz);
                    this.lifecycleMetadataCache.put(clazz, metadata);
                }
                return metadata;
            }
        }
        return metadata;
    }

    private LifecycleMetadata buildLifecycleMetadata(final Class<?> clazz) {
        if (!AnnotationUtils.isCandidateClass(clazz, Arrays.asList(this.initAnnotationType, this.destroyAnnotationType))) {
            return this.emptyLifecycleMetadata;
        }

        List<LifecycleElement> initMethods = new ArrayList<>();
        List<LifecycleElement> destroyMethods = new ArrayList<>();
        Class<?> targetClass = clazz;

        do {
            final List<LifecycleElement> currInitMethods = new ArrayList<>();
            final List<LifecycleElement> currDestroyMethods = new ArrayList<>();

            ReflectionUtils.doWithLocalMethods(targetClass, method -> {
                if (this.initAnnotationType != null && method.isAnnotationPresent(this.initAnnotationType)) {
                    LifecycleElement element = new LifecycleElement(method);
                    currInitMethods.add(element);
                    if (log.isTraceEnabled()) {
                        log.trace("Found init method on class [" + clazz.getName() + "]: " + method);
                    }
                }
                if (this.destroyAnnotationType != null && method.isAnnotationPresent(this.destroyAnnotationType)) {
                    currDestroyMethods.add(new LifecycleElement(method));
                    if (log.isTraceEnabled()) {
                        log.trace("Found destroy method on class [" + clazz.getName() + "]: " + method);
                    }
                }
            });

            initMethods.addAll(0, currInitMethods);
            destroyMethods.addAll(currDestroyMethods);
            targetClass = targetClass.getSuperclass();
        }
        while (targetClass != null && targetClass != Object.class);

        return (initMethods.isEmpty() && destroyMethods.isEmpty() ? this.emptyLifecycleMetadata :
                new LifecycleMetadata(clazz, initMethods, destroyMethods));
    }

    /**
     * Class representing information about annotated init and destroy methods.
     */
    private class LifecycleMetadata {

        private final Class<?> targetClass;

        private final Collection<LifecycleElement> initMethods;

        private final Collection<LifecycleElement> destroyMethods;

        @Nullable
        private volatile Set<LifecycleElement> checkedInitMethods;

        @Nullable
        private volatile Set<LifecycleElement> checkedDestroyMethods;

        public LifecycleMetadata(Class<?> targetClass, Collection<LifecycleElement> initMethods,
                                 Collection<LifecycleElement> destroyMethods) {

            this.targetClass = targetClass;
            this.initMethods = initMethods;
            this.destroyMethods = destroyMethods;
        }

        public void checkConfigMembers(RootBeanDefinition beanDefinition) {
            Set<LifecycleElement> checkedInitMethods = new LinkedHashSet<>(this.initMethods.size());
            for (LifecycleElement element : this.initMethods) {
                String methodIdentifier = element.getIdentifier();
                if (!beanDefinition.isExternallyManagedInitMethod(methodIdentifier)) {
                    beanDefinition.registerExternallyManagedInitMethod(methodIdentifier);
                    checkedInitMethods.add(element);
                    if (log.isTraceEnabled()) {
                        log.trace("Registered init method on class [" + this.targetClass.getName() + "]: " + element);
                    }
                }
            }
            Set<LifecycleElement> checkedDestroyMethods = new LinkedHashSet<>(this.destroyMethods.size());
            for (LifecycleElement element : this.destroyMethods) {
                String methodIdentifier = element.getIdentifier();
                if (!beanDefinition.isExternallyManagedDestroyMethod(methodIdentifier)) {
                    beanDefinition.registerExternallyManagedDestroyMethod(methodIdentifier);
                    checkedDestroyMethods.add(element);
                    if (log.isTraceEnabled()) {
                        log.trace("Registered destroy method on class [" + this.targetClass.getName() + "]: " + element);
                    }
                }
            }
            this.checkedInitMethods = checkedInitMethods;
            this.checkedDestroyMethods = checkedDestroyMethods;
        }

        public void invokeInitMethods(Object target, String beanName) throws Throwable {
            Collection<LifecycleElement> checkedInitMethods = this.checkedInitMethods;
            Collection<LifecycleElement> initMethodsToIterate =
                    (checkedInitMethods != null ? checkedInitMethods : this.initMethods);
            if (!initMethodsToIterate.isEmpty()) {
                for (LifecycleElement element : initMethodsToIterate) {
                    if (log.isTraceEnabled()) {
                        log.trace("Invoking init method on bean '" + beanName + "': " + element.getMethod());
                    }
                    element.invoke(target);
                }
            }
        }

        public void invokeDestroyMethods(Object target, String beanName) throws Throwable {
            Collection<LifecycleElement> checkedDestroyMethods = this.checkedDestroyMethods;
            Collection<LifecycleElement> destroyMethodsToUse =
                    (checkedDestroyMethods != null ? checkedDestroyMethods : this.destroyMethods);
            if (!destroyMethodsToUse.isEmpty()) {
                for (LifecycleElement element : destroyMethodsToUse) {
                    if (log.isTraceEnabled()) {
                        log.trace("Invoking destroy method on bean '" + beanName + "': " + element.getMethod());
                    }
                    element.invoke(target);
                }
            }
        }

        public boolean hasDestroyMethods() {
            Collection<LifecycleElement> checkedDestroyMethods = this.checkedDestroyMethods;
            Collection<LifecycleElement> destroyMethodsToUse =
                    (checkedDestroyMethods != null ? checkedDestroyMethods : this.destroyMethods);
            return !destroyMethodsToUse.isEmpty();
        }
    }


    /**
     * Class representing injection information about an annotated method.
     */
    private static class LifecycleElement {

        private final Method method;

        private final String identifier;

        public LifecycleElement(Method method) {
            if (method.getParameterCount() != 0) {
                throw new IllegalStateException("Lifecycle method annotation requires a no-arg method: " + method);
            }
            this.method = method;
            this.identifier = (Modifier.isPrivate(method.getModifiers()) ?
                    ClassUtils.getQualifiedMethodName(method) : method.getName());
        }

        public Method getMethod() {
            return this.method;
        }

        public String getIdentifier() {
            return this.identifier;
        }

        public void invoke(Object target) throws Throwable {
            ReflectionUtils.makeAccessible(this.method);
            this.method.invoke(target, (Object[]) null);
        }

        @Override
        public boolean equals(@Nullable Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof LifecycleElement)) {
                return false;
            }
            LifecycleElement otherElement = (LifecycleElement) other;
            return (this.identifier.equals(otherElement.identifier));
        }

        @Override
        public int hashCode() {
            return this.identifier.hashCode();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        log.debug(getClass().getSimpleName()+"："+getPathName()+"正在finalize");
        if(isDeleteJar) {
            try {
                FileUtil.del(pathName);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}
