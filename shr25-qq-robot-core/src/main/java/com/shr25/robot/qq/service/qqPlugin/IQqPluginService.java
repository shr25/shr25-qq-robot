package com.shr25.robot.qq.service.qqPlugin;

import com.shr25.robot.base.MyBaseService;
import com.shr25.robot.qq.model.qqPlugin.QqPlugin;

/**
 * QQ群插件Service接口
 *
 * @author huobing
 * @date 2022-06-14 14:12:10
*/
public interface IQqPluginService extends MyBaseService<QqPlugin> {

    /**
     * 使用插件
     * @param id
     */
    void enabled(Long id);

    /**
     *删除插件（不使用）
     * @param id
     */
    void disable(Long id);
}
