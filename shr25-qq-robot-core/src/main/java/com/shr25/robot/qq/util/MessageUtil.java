package com.shr25.robot.qq.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.ranges.LongRange;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.Mirai;
import net.mamoe.mirai.contact.*;
import net.mamoe.mirai.data.MemberInfo;
import net.mamoe.mirai.data.UserProfile;
import net.mamoe.mirai.event.events.MessageEvent;
import net.mamoe.mirai.internal.GroupUtils;
import net.mamoe.mirai.message.data.*;
import net.mamoe.mirai.utils.ExternalResource;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 消息处理工具
 *
 * @author huobing
 * @date 2022/6/3 21:25
 */
@Slf4j
public class MessageUtil {

    /**
     * 创建一个消息构造器
     *
     * @return {@link MessageChainBuilder}
     */
    public static MessageChainBuilder createBuilder() {
        return new MessageChainBuilder();
    }

    /**
     * 创建一个回复消息构造器
     *
     * @return {@link MessageChainBuilder}
     */
    public static MessageChainBuilder createQuoteReplyBuilder(MessageChain sourceMessage) {
        QuoteReply quoteReply = new  QuoteReply(sourceMessage);
        MessageChainBuilder messageChainBuilder = new MessageChainBuilder()
                .append(quoteReply);
        return messageChainBuilder;
    }

    /**
     * 创建一个回复并At消息构造器
     *
     * @return {@link MessageChainBuilder}
     */
    public static MessageChainBuilder createQuoteReplyAndAtBuilder(MessageChain sourceMessage, Long qq) {
        MessageChainBuilder messageChainBuilder = createQuoteReplyBuilder(sourceMessage)
                .append(new At(qq));
        return messageChainBuilder;
    }

    /**
     * 创建一个回复并At消息构造器
     *
     * @return {@link MessageChainBuilder}
     */
    public static MessageChainBuilder createQuoteReplyBuilder(MessageEvent messageEvent) {
        MessageChainBuilder messageChainBuilder = createQuoteReplyBuilder(messageEvent.getMessage());
        return messageChainBuilder;
    }

    /**
     * 创建一个回复并At消息构造器
     *
     * @return {@link MessageChainBuilder}
     */
    public static MessageChainBuilder createQuoteReplyAndAtBuilder(MessageEvent messageEvent) {
        MessageChainBuilder messageChainBuilder = createQuoteReplyBuilder(messageEvent.getMessage())
                .append(new At(messageEvent.getSender().getId()));
        return messageChainBuilder;
    }



    /**
     * 构建图片消息
     *
     * @param sender Group Or Friend Or Member 对象
     * @param image  图片文件
     * @return 构建的图片消息
     */
    public static Image buildImageMessage(Contact sender, File image) {
        ExternalResource externalImage = null;
        try {
            externalImage = ExternalResource.create(image);
            return sender.uploadImage(externalImage);
        } catch (Exception e) {
            log.error("文件消息转换失败", e);
        } finally {
            if (externalImage != null) {
                try {
                    externalImage.close();
                } catch (IOException e) {
                    log.error("externalImage关闭失败", e);
                }
            }
        }
        return null;
    }

    /**
     * 构建图片消息
     *
     * @param sender Group Or Friend Or Member 对象
     * @param image  图片文件
     * @return 构建的图片消息
     */
    public static Image buildImageMessage(Contact sender, InputStream image) {
        ExternalResource externalImage = null;
        try {
            externalImage = ExternalResource.create(image);
            return sender.uploadImage(externalImage);
        } catch (Exception e) {
            log.error("文件消息转换失败", e);
        } finally {
            if (externalImage != null) {
                try {
                    externalImage.close();
                } catch (IOException e) {
                    log.error("externalImage关闭失败", e);
                }
            }
        }
        return null;
    }

    /**
     * 获取远程文件
     *
     * @param imageId  mirai中的文件id
     * @return 构建的图片消息
     */
    public static File getImage(String imageId) throws IOException {
        String fileName = imageId.replace("{","").replace("}","").replace("-", "");
        String prefix = fileName.substring(0, fileName.lastIndexOf("."));
        String suffix = fileName.substring(prefix.length()-1,fileName.length());
        String url = "https://gchat.qpic.cn/gchatpic_new/0/0-0-"+prefix+"/0";
        HttpResponse response = HttpRequest.get(url).setFollowRedirects(true).execute();
        File file = File.createTempFile(prefix,suffix,new File(System.getProperty("java.io.tmpdir")));
        FileUtil.writeBytes(response.bodyBytes(), file);
        return file;
    }

    /**
     * 获取一个随机的文件，自动判断是否在文件夹
     *
     * @param file 需要获取的文件夹
     * @return 获取到的文件，可能为null
     */
    public static File randomFile(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                return file;
            } else if (file.isDirectory()) {
                File[] imgList = file.listFiles();
                if (imgList != null && imgList.length > 0) {
                    return randomFile(RandomUtil.randomEle(imgList));
                }
            }
        }
        return null;
    }

    /**
     * 获取指定的图片
     * @param dir
     * @param img
     * @return
     */
    public static File getFile(File dir, String img) {
        if (dir.exists()) {
            if (dir.isDirectory()) {
                File imgFile = new File(dir.getAbsolutePath()+File.separator+img);
                if (imgFile.exists()) {
                    return imgFile;
                }
            }
        }
        return null;
    }

    /**
     * 直接发送群消息
     * @param groupId
     * @param message
     */
    public static void sendGroupMessage(Long groupId, String message){
        sendGroupMessage(groupId, new PlainText(message));
    }

    /**
     * 直接发送群消息
     * @param groupId
     * @param message
     */
    public static void sendGroupMessage(Long groupId, Message message){
        Group group = getGroup(groupId);
        if(group != null){
            group.sendMessage(message);
        }
    }

    /**
     * 获取QQ群员
     * @param groupId
     * @param qq
     */
    public static NormalMember getGroupMember(Long groupId, Long qq){
        Group group = getGroup(groupId);
        if(group != null){
            return group.get(qq);
        }
        return null;
    }

    /**
     * 获取QQ群员
     * @param groupId
     * @param nick
     */
    public static NormalMember getGroupMember(Long groupId, String nick){
        AtomicReference<NormalMember> member = new AtomicReference<>();
        Group group = getGroup(groupId);
        if(group != null){
            group.getMembers().forEach(normalMember -> {
                if(normalMember.getNick().equals(nick)){
                    member.set(normalMember);
                }
            });
        }
        return member.get();
    }

    /**
     * 获取QQ群员
     * @param group
     */
    public static NormalMember getGroupMember(Group group, Long qq){
        if(group != null){
            return group.get(qq);
        }
        return null;
    }

    /**
     * 获取QQ群员
     * @param group
     * @param nick
     */
    public static NormalMember getGroupMember(Group group, String nick){
        AtomicReference<NormalMember> member = new AtomicReference<>();
        if(group != null){
            group.getMembers().forEach(normalMember -> {
                if(normalMember.getNick().equals(nick)){
                    member.set(normalMember);
                }
            });
        }
        return member.get();
    }

    /**
     * 获取群员信息
     * @param groupId
     * @param qq
     * @return
     */
    public static MemberInfo getGroupMemberInfo(Long groupId, Long qq){
        return getGroupMemberInfo(getGroupMember(groupId,qq));
    }

    /**
     * 获取群员信息
     * @param group
     * @param qq
     * @return
     */
    public static MemberInfo getGroupMemberInfo(Group group, Long qq){
        return getGroupMemberInfo(getGroupMember(group,qq));
    }

    /**
     * 获取群员信息
     * @param normalMember
     * @return
     */
    public static MemberInfo getGroupMemberInfo(NormalMember normalMember){
        MemberInfo memberInfo = (MemberInfo) ReflectUtil.getFieldValue(normalMember, "info");
        return memberInfo;
    }

    /**
     * 默认系统机器人号段
     */
    private static LongRange sysBotRange = new LongRange(2854196306L, 2854216398L);

    /**
     * 是否系统机器人
     * @param normalMember
     * @return
     */
    public static boolean isSysBot(NormalMember normalMember){
        MemberInfo memberInfo = MessageUtil.getGroupMemberInfo(normalMember);
        return memberInfo.isOfficialBot() || isSysBot(normalMember.getId());
    }

    /**
     * 是否系统机器人
     * @param groupId
     * @param qq
     * @return
     */
    public static boolean isSysBot(Long groupId, Long qq){
        MemberInfo memberInfo = MessageUtil.getGroupMemberInfo(groupId, qq);
        return memberInfo.isOfficialBot() || isSysBot(qq);
    }

    /**
     * 是否系统机器人
     * @param qq
     * @return
     */
    public static boolean isSysBot(Long qq){
        return sysBotRange.contains(qq);
    }

    /**
     * 获取好友
     * @param qq
     */
    public static Friend getFriend(Long qq){
        return Bot.getInstances().get(0).getFriend(qq);
    }

    /**
     * 获取陌生人
     * @param qq
     */
    public static Stranger getStranger(Long qq){
        return Bot.getInstances().get(0).getStranger(qq);
    }

    /**
     * 获取用户信息
     * @param qq
     * @return
     */
    public static UserProfile getQqInfo(Long qq){
        return Mirai.getInstance().queryProfile(Bot.getInstances().get(0), qq);
    }

    public static Integer getMaxGroupMemberNum(Long groupId, Bot bot){
        JSONObject info = getGroupInfo(groupId, bot);
       if(info != null){
           return info.getInt("dwMaxGroupMemberNum");
       }
        return null;
    }

    public static JSONObject getGroupInfo(Long groupId, Bot bot){
        JSONObject info = null;
        try {
            JSONArray groups = getGroupInfos(bot);
            if(groups != null && groups.size()>0){
                for (int i = 0; i < groups.size(); i++) {
                    if(groupId.equals(groups.getJSONObject(i).getLong("groupCode"))){
                        info = groups.getJSONObject(i);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return info;
    }

    /**
     * 获取 QQ群信息和
     * @param bot
     * @return
     */
    public static JSONArray getGroupInfos(Bot bot){
        JSONArray groups = null;
        try {
            JSONObject data = getGroups(bot);
            groups = data.getJSONArray("groups");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return groups;
    }

    /**
     * 获取 QQ群信息和
     * @param bot
     * @return
     */
    public static JSONObject getGroups(Bot bot){
        JSONObject reData = null;
        try {
            Object outgoingPacketFactory = GroupUtils.getGetTroopListSimplify(bot);
            Object data = sendAndExpect(bot, outgoingPacketFactory);
            reData = JSONUtil.parseObj(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return reData;
    }

    public static Object sendAndExpect(Bot bot, Object... args) throws InvocationTargetException, IllegalAccessException {
        final Object[] data = {null};
        Object network = GroupUtils.getNetwork(bot);
        Method sendAndExpect = ReflectUtil.getMethodByName(network.getClass(), "sendAndExpect");
        Object[] params = new Object[args.length+3];
        for (int i = 0; i < args.length; i++) {
            params[i] = args[i];
        }
        params[args.length] = 5000;
        params[args.length+1] = 3;
        params[args.length+2] = new Continuation<Object>() {
            @Override
            public void resumeWith(@NotNull Object o) {
                data[0] = o;
                //拿数据
            }

            @Override
            public CoroutineContext getContext() {
                return EmptyCoroutineContext.INSTANCE;
            }
        };
        sendAndExpect.invoke(network, params);
        Long currTime = System.currentTimeMillis();
        while (data[0] == null && System.currentTimeMillis() - currTime <= 5000){

        }
        return data[0];
    }

    /**
     * 缓存每个群的负责bot
     */
    private static ConcurrentHashMap<Long, Long> ceachGroupBot = new ConcurrentHashMap<>();

    /**
     * 添加群负责Bot
     * @param bot
     */
    public static void addGroups(Bot bot) {
        //初始化数据
        //bot负责的群数量
        Map<Long,Integer> botGroupCount = new HashMap<>();
        ceachGroupBot.forEach((groupId, botId) -> {
            Integer count = botGroupCount.get(botId);
            if(count == null){
                count = 1;
            }else{
                count++;
            }
            botGroupCount.put(botId, count);
        });

        Long botId = bot.getId();
        Integer count = botGroupCount.get(botId);
        if(count == null){
            botGroupCount.put(botId, 0);
        }

        bot.getGroups().forEach(group -> {
            //给群设置负责的机器人
            Long oldBotId = ceachGroupBot.get(group.getId());
            if(oldBotId != null){
                Group oldGroup = null;
                Bot oldBot = Bot.getInstanceOrNull(oldBotId);
                if(oldBot != null){
                    oldGroup = oldBot.getGroup(group.getId());
                }
                if(oldGroup != null && group.getBotPermission().getLevel() > oldGroup.getBotPermission().getLevel()){
                    Integer firstCount = botGroupCount.get(botId);
                    Integer secondCount = botGroupCount.get(oldBot.getId());
                    if(firstCount < secondCount){
                        ceachGroupBot.put(group.getId(), botId);
                        botGroupCount.put(botId, firstCount +1);
                        botGroupCount.put(oldBotId, secondCount -1);
                    }
                }
            }else{
                ceachGroupBot.put(group.getId(), group.getBot().getId());
                botGroupCount.put(botId, botGroupCount.get(botId)+1);
            }

        });
    }

    /**
     * 获取QQ群
     */
    public static ContactList<Group> getAllGroups(){
        List<Group> groups = new ArrayList<>();
        ceachGroupBot.forEach((groupId, bootId)->{
            Group group = null;
            try{
                group = Bot.getInstanceOrNull(bootId).getGroup(groupId);
            }catch (Exception e){
                group = getGroup(groupId);
            }
            if(group != null){
                groups.add(group);
            }
        });
        return new ContactList(groups);
    }

    /**
     * 获取QQ群
     * @param groupId
     */
    public static Group getGroup(Long groupId){
        Group group = null;
        for (Bot bot : Bot.getInstances()) {
            Group oldGroup = bot.getGroup(groupId);
            if(oldGroup != null){
                group = oldGroup;
            }
            if(ceachGroupBot.contains(groupId) && ceachGroupBot.get(groupId).compareTo(bot.getId()) == 0){
                break;
            }
        }

        return group;
    }
    public static boolean isMyGroup(Group group){
        Long botId = group.getBot().getId();
        if(ceachGroupBot.contains(group.getId())) {
            return botId.compareTo(ceachGroupBot.get(group.getId())) == 0;
        }else{
            Group oldGroup = getGroup(group.getId());
            return botId.compareTo(oldGroup.getBot().getId()) == 0;
        }
    }

    public static void joinGroup(Group group) {
        Long oldBotId = ceachGroupBot.get(group.getId());
        if(oldBotId == null){
            ceachGroupBot.put(group.getId(), group.getBot().getId());
        }
    }

    public static void leaveGroup(Group group) {
        Long oldBotId = group.getBot().getId();
        Long botId = ceachGroupBot.get(group.getId());
        if(oldBotId.compareTo(botId) == 0){
            ceachGroupBot.remove(group.getId());
            //bot负责的群数量
            Map<Long,Integer> botGroupCount = new HashMap<>();
            ceachGroupBot.forEach((groupId, botQqId) -> {
                Integer count = botGroupCount.get(botQqId);
                if(count == null){
                    count = 1;
                }else{
                    count++;
                }
                botGroupCount.put(botQqId, count);
            });
            Group newGroup = null;
            for (Bot bot : Bot.getInstances()) {
                if (oldBotId.compareTo(bot.getId()) != 0) {
                    //给群设置负责的机器人
                    Group oldGroup = bot.getGroup(group.getId());
                    if(oldGroup != null) {
                        if (newGroup == null) {
                            newGroup = oldGroup;
                        } else if (oldGroup.getBotPermission().getLevel() > newGroup.getBotPermission().getLevel()) {
                            Integer firstCount = botGroupCount.get(newGroup.getBot().getId());
                            Integer secondCount = botGroupCount.get(bot.getId());
                            if (firstCount > secondCount) {
                                newGroup = oldGroup;
                            }
                        }
                    }
                }
            }

            if(newGroup != null){
                ceachGroupBot.put(newGroup.getId(), newGroup.getBot().getId());
            }
        }
    }
}
