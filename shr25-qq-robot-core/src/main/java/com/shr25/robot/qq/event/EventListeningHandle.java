package com.shr25.robot.qq.event;

import com.shr25.robot.qq.conf.QqConfig;
import com.shr25.robot.qq.model.QqGroupInfo;
import com.shr25.robot.qq.service.RobotManagerService;
import com.shr25.robot.qq.service.qqGroup.IQqGroupInfoService;
import com.shr25.robot.qq.util.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.ContactList;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.event.EventHandler;
import net.mamoe.mirai.event.ListeningStatus;
import net.mamoe.mirai.event.SimpleListenerHost;
import net.mamoe.mirai.event.events.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * @author huobing
 * @date 2022/6/3 21:25
 */
@Slf4j
@Component
public class EventListeningHandle extends SimpleListenerHost {
    @Autowired
    private RobotManagerService robotManagerService;

    @Autowired
    IQqGroupInfoService qqGroupInfoService;

    @Autowired
    private QqConfig qqConfig;

    /**
     * 监听机器人消息消息
     *
     * @param event 群消息
     * @return {@link ListeningStatus}
     */
    @EventHandler
    public ListeningStatus onMessageEvent(BotEvent event) {
        this.publishMessage(event);
        // 保持监听
        return ListeningStatus.LISTENING;
    }


    private void publishMessage(BotEvent event) {
        try {
            if(event instanceof BotOnlineEvent){
                event.getBot().getGroups().forEach(group -> {
                    QqGroupInfo old = qqGroupInfoService.getByGroupId(group.getId());
                    if(old == null){
                        QqGroupInfo qqGroupInfo = new QqGroupInfo();
                        qqGroupInfo.setGroupId(group.getId());
                        qqGroupInfo.setGroupName(group.getName());
                        qqGroupInfoService.save(qqGroupInfo);
                    }
                });

                MessageUtil.addGroups(event.getBot());
            }else if(event instanceof BotJoinGroupEvent){
                MessageUtil.joinGroup(((BotJoinGroupEvent) event).getGroup());
                robotManagerService.publishMessage(event);
            }else if(event instanceof BotLeaveEvent){
                MessageUtil.leaveGroup(((BotLeaveEvent) event).getGroup());
                robotManagerService.publishMessage(event);
            }else if(event instanceof GroupMessageSyncEvent){
                publishGroupMessage(((GroupMessageSyncEvent) event).getGroup(),event);
            }else if(event instanceof GroupMessageEvent){
                publishGroupMessage(((GroupMessageEvent) event).getGroup(),event);
            }else if(event instanceof GroupMemberEvent){
                publishGroupMessage(((GroupMemberEvent) event).getGroup(),event);
            }else if(event instanceof MemberJoinRequestEvent){
                publishGroupMessage(((MemberJoinRequestEvent) event).getGroup(),event);
            }else {
                robotManagerService.publishMessage(event);
            }
        } catch (Exception e) {
            log.error(String.format("发送消息失败:%s", e.getMessage()), e);
        }
    }

    /**
     * 处理群消息
     * @param group
     * @param event
     */
    public void publishGroupMessage(Group group, BotEvent event){
        if(MessageUtil.isMyGroup(group)) {
            robotManagerService.publishMessage(event);
        }
    }
}
