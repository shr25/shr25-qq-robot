package com.shr25.robot.qq.service.qqGroup;

import com.shr25.robot.base.MyBaseService;
import com.shr25.robot.qq.model.QqGroupInfo;

import java.util.List;

/**
 * QQ群信息Service接口
 *
 * @author huobing
 * @date 2022-6-20 16:36
 */
public interface IQqGroupInfoService extends MyBaseService<QqGroupInfo> {

    /**
     * 通过群id获取群
     * @param groupId
     * @return
     */
    QqGroupInfo getByGroupId(Long groupId);

    /**
     * 通过分类获取群
     * @param keyword
     * @return
     */
    List<QqGroupInfo> findByKeyword(String keyword);
}
