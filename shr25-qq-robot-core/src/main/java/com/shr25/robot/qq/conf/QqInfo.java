package com.shr25.robot.qq.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * qq账号登录信息
 *
 * @author huobing
 * @date 2022/6/3 21:25
 */
@Data
public class QqInfo {
    /** 机器人QQ号 */
    private Long qq;
    /** 机器人密码 */
    private String password;
    /** 使用协议 ANDROID_PHONE,  ANDROID_PAD, ANDROID_WATCH, IPAD, MACOS */
    private String protocol;
    /** 是否扫码登录 */
    private boolean loginByQr = false;
}
