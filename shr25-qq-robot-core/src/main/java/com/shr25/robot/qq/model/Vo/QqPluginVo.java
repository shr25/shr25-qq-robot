package com.shr25.robot.qq.model.Vo;

import com.shr25.robot.qq.plugins.RobotPlugin;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * qq插件配置
 *
 * @author huobing
 * @date 2022-6-14 14:53
 */
@Data
public class QqPluginVo implements Comparable<QqPluginVo>{
  /** QQ群插件关系id */
  private Long id;

  /** 插件id */
  private Long pluginId;

  /** 插件自定义名称 */
  private String name;

  /** 插件描述 */
  private String pluginDesc;

  /** 排序 */
  private Integer sort;

  /** 是否所有都可以使用 */
  private Integer isAll;

  /** 是否启用 */
  private boolean enabled;

  /** 插件类名 */
  private String className;

  /** 插件版本号 */
  private String version;

  /** 公共插件需要排除的群号 */
  private Set<Long> notGroups = new HashSet<>();

  public void setPluginEnabled(Integer itemEnabled){
      this.enabled = itemEnabled != null && itemEnabled > 0;
  }

  public void addNotGroups(Long groupId){
    notGroups.add(groupId);
  }

  public void removeNotGroups(Long groupId){
    notGroups.remove(groupId);
  }

  @Override
  public int compareTo(@NotNull QqPluginVo qqPluginVo) {
    if (getSort() == qqPluginVo.getSort()) {
      return 0;
    }
    return getSort() > qqPluginVo.getSort() ? 1 : -1;
  }

  @Override
  public int hashCode() {
    return pluginId.hashCode();
  }

  @Override
  public boolean equals(Object qqPluginVo) {
    return pluginId == ((QqPluginVo)qqPluginVo).getPluginId();
  }
}
