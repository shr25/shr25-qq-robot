package com.shr25.robot.qq.service.qqGroup.impl;

import com.shr25.robot.base.MyBaseServiceImpl;
import com.shr25.robot.qq.mapper.qqGroup.QqGroupPluginMapper;
import com.shr25.robot.qq.model.QqGroupPlugin;
import com.shr25.robot.qq.model.Vo.QqPluginVo;
import com.shr25.robot.qq.service.qqGroup.IQqGroupPluginService;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.contact.User;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * QQ群插件Service业务层处理
 *
 * @author huobing
 * @date 2022-06-14 14:13:04
 */

@Slf4j
@Service
public class QqGroupPluginServiceImpl extends MyBaseServiceImpl<QqGroupPluginMapper, QqGroupPlugin> implements IQqGroupPluginService {

    @Override
    public QqGroupPlugin getGroupPlugin(Long groupId, Long pluginid) {
        return lambdaQuery().eq(QqGroupPlugin::getGroupId, groupId).eq(QqGroupPlugin::getPluginId, pluginid).one();
    }

    @Override
    public void enabled(Long id) {
        lambdaUpdate().set(QqGroupPlugin::getEnabled, 1).eq(QqGroupPlugin::getId, id).update();
    }

    @Override
    public void disable(Long id) {
        lambdaUpdate().set(QqGroupPlugin::getEnabled, 0).eq(QqGroupPlugin::getId, id).update();
    }

    @Override
    public QqGroupPlugin addQqGroupPlugin(Long groupId, Boolean enabled, QqPluginVo qqPluginVo, User sender) {
        QqGroupPlugin qqGroupPlugin = new QqGroupPlugin();
        qqGroupPlugin.setPluginId(qqPluginVo.getPluginId());
        qqGroupPlugin.setGroupId(groupId);
        qqGroupPlugin.setEnabled(enabled ? 1 : 0);
        qqGroupPlugin.setSort(qqPluginVo.getSort());
        qqGroupPlugin.setCreateId(sender.getId());
        qqGroupPlugin.setCreateBy(sender.getNick());
        qqGroupPlugin.setCreateTime(new Date());
        save(qqGroupPlugin);
        return qqGroupPlugin;
    }
}
