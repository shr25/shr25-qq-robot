package com.shr25.robot.qq.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.shr25.robot.classLoader.Shr25PluginClassLoad;
import com.shr25.robot.qq.conf.QqConfig;
import com.shr25.robot.qq.model.QqGroupLog;
import com.shr25.robot.qq.model.QqGroupPlugin;
import com.shr25.robot.qq.model.QqMessage;
import com.shr25.robot.qq.model.Vo.QqPluginVo;
import com.shr25.robot.qq.model.qqPlugin.QqPlugin;
import com.shr25.robot.qq.plugins.RobotPlugin;
import com.shr25.robot.qq.service.qqGroup.IQqGroupLogService;
import com.shr25.robot.qq.service.qqGroup.IQqGroupPluginService;
import com.shr25.robot.qq.service.qqPlugin.IQqPluginService;
import com.shr25.robot.qq.util.MessageUtil;
import com.shr25.robot.utils.ManifestUtils;
import lombok.extern.slf4j.Slf4j;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.event.events.*;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.io.Shr25ClassLoaderWrapper;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

/**
 * 机器人服务类
 *
 * @author huobing
 * @date 2022/6/3 21:25
 */
@Service
@Slf4j
public class RobotManagerService {
    /** 版本号 */
    private String version;

    /**
     * 插件目录
     */
    private File pluginsDir = new File("plugins");

    /**
     * 插件状态模板
     */
    private final String template = "名称：%s 启动：%s\n";

    @Autowired
    private IQqGroupPluginService qqGroupPluginService;

    @Autowired
    private IQqPluginService qqPluginService;

    @Autowired
    IQqGroupLogService qqGroupLogService;

    @Autowired
    private QqConfig qqConfig;

    /**
     * 是否开启
     */
    private boolean enabled = true;

    /**
     * 普通管理员qq列表
     */
    private static final Set<Long> NORMAL_MANAGE_QQ = new HashSet<>();

    /**
     * 根据群号隔离每个插件的启动
     */
    private final Map<Long, List<QqPluginVo>> groupPluginMap = new HashMap<>();

    /**
     * 公共插件
     */
    private List<QqPluginVo> publickPlugins;

    /**
     * 所以插件
     */
    private List<QqPluginVo> allPluginVos;

    /**
     * 缓存qq插件  插件id => 插件
     */
    private final Map<Long, QqPluginVo> qqPluginIdMap = new HashMap<>();

    /**
     * 缓存qq插件  插件名称 => 插件
     */
    private final Map<String, QqPluginVo> qqPluginNameMap = new HashMap<>();

    /**
     * 机器人插件缓存 插件类名 => 插件
     */
    private final Map<String, RobotPlugin> robotPluginMap = new HashMap<>();

    /**
     * 缓存qq插件  插件类名 => 插件加载器
     */
    private final Map<String, Shr25PluginClassLoad> shr25PluginClassLoadMap = new HashMap<>();

    /**
     * 初始化插件缓存
     */
    @PostConstruct
    public void initAllRobotPlugin() {
        getVersion();
        log.info("SHR25-QQ机器人“{}”", StringUtils.isBlank(getVersion())? "插件打包后才可以显示版本" : "V"+getVersion());
        //根目录
        if(pluginsDir.exists() && pluginsDir.isDirectory()){
            for (File file : pluginsDir.listFiles()) {
                try {
                    Shr25PluginClassLoad loader = new Shr25PluginClassLoad(file);
                    for (RobotPlugin robotPlugin : loader.getRobotPlugins()) {
                        shr25PluginClassLoadMap.put(robotPlugin.getClass().getName(), loader);
                    }
                }catch (Exception e){
                    log.error("加载插件失败："+file.getAbsolutePath(), e);
                }
            }
        }
        initRobotPlugin();
    }

    /**
     * 加载插件
     * @param jaFileName
     */
    public void addRobotPluginJar(String jaFileName) {
        log.info("加载插件：{}", jaFileName);
        //更目录
        File file = new File(pluginsDir.getAbsolutePath()+File.separator+jaFileName+".jar");
        if(file.exists()){
            log.info("加载插件：{}---{}", jaFileName, "插件文件存在，检查是否已加载插件");
            if(Shr25ClassLoaderWrapper.hasShr25PluginClassLoader(file.getAbsolutePath())){
                log.info("加载插件：{}---{}", jaFileName, "插件存在，开始卸载");
                destroyRobotPluginJar(Shr25ClassLoaderWrapper.getShr25PluginClassLoader(file.getAbsolutePath()), false);
                log.info("加载插件：{}---{}", jaFileName, "插件存在，卸载完毕");
            }
            try{
                log.info("加载插件：{}---{}", jaFileName, "开始加载插件");
                Shr25PluginClassLoad loader = new Shr25PluginClassLoad(file);
                for (RobotPlugin robotPlugins : loader.getRobotPlugins()) {
                    shr25PluginClassLoadMap.put(robotPlugins.getClass().getName(), loader);
                }
                log.info("加载插件：{}---{}", jaFileName, "加载插件完毕");
            }catch (Exception e){
                log.error("加载插件失败："+file.getAbsolutePath(), e);
            }
        }else{
            log.info("加载插件：{}---{}", jaFileName, "插件文件不存在");
        }
        initRobotPlugin();
    }

    /**
     * 切换插件版本
     * @param qqPluginName
     */
    public void updateRobotPluginJar(String qqPluginName) {
        updateRobotPluginJar(qqPluginNameMap.get(qqPluginName));
    }

    /**
     * 切换插件版本
     * @param qqPluginVo
     */
    private void updateRobotPluginJar(QqPluginVo qqPluginVo) {
        if(qqPluginVo != null) {
            Shr25PluginClassLoad shr25PluginClassLoad = shr25PluginClassLoadMap.get(qqPluginVo.getClassName());
            log.info("切换插件：{}", qqPluginVo.getName());
            if (shr25PluginClassLoad != null) {
                String pathName = shr25PluginClassLoad.getPathName();
                log.info("切换插件：{}---{}", qqPluginVo.getName(), "插件存在，开始卸载插件");
                String oldJarName = shr25PluginClassLoad.getPathName().substring(shr25PluginClassLoad.getPathName().lastIndexOf(File.separator)+1);
                destroyRobotPluginJar(shr25PluginClassLoad, true);
                shr25PluginClassLoad = null;
                log.info("切换插件：{}---{}", qqPluginVo.getName(), "插件存在，卸载完成");
                oldJarName = oldJarName.replaceAll("-\\d+\\.\\d+\\.\\d+\\.jar", "");
                //根目录
                if (pluginsDir.exists() && pluginsDir.isDirectory()) {
                    for (File file : pluginsDir.listFiles()) {
                        String jarName = file.getName().replaceAll("-\\d+\\.\\d+\\.\\d+\\.jar", "");
                        if (oldJarName.equals(jarName) && !pathName.equals(file.getAbsolutePath())) {
                            log.info("切换插件：{}---{}", qqPluginVo.getName(), "开始加载新版本插件");
                            try{
                                Shr25PluginClassLoad loader = new Shr25PluginClassLoad(file);
                                for (RobotPlugin robotPlugins : loader.getRobotPlugins()) {
                                    shr25PluginClassLoadMap.put(robotPlugins.getClass().getName(), loader);
                                }
                                log.info("切换插件：{}---{}", qqPluginVo.getName(), "新版本插件加载完成");
                            }catch (Exception e){
                                log.error("加载插件失败："+file.getAbsolutePath(), e);
                            }
                            break;
                        }
                    }
                }
            }else{
                log.info("切换插件：{}---{}", qqPluginVo.getName(), "插件不存在");
            }
            initRobotPlugin();
        }
    }

    /**
     * 卸载插件
     * @param qqPluginName
     */
    public void destroyRobotPluginJar(String qqPluginName) {
        destroyRobotPluginJar(qqPluginNameMap.get(qqPluginName));
    }

    /**
     * 卸载插件
     * @param qqPluginVo
     */
    private void destroyRobotPluginJar(QqPluginVo qqPluginVo) {
        if(qqPluginVo != null) {
            Shr25PluginClassLoad shr25PluginClassLoad = shr25PluginClassLoadMap.get(qqPluginVo.getClassName());
            log.info("卸载插件：{}", qqPluginVo.getName());
            if (shr25PluginClassLoad != null) {
                log.info("卸载插件：{}---{}", qqPluginVo.getName(), "插件存在，开始卸载插件");
                destroyRobotPluginJar(shr25PluginClassLoad, true);
                shr25PluginClassLoad = null;
                log.info("卸载插件：{}---{}", qqPluginVo.getName(), "插件存在，卸载完成");
            }else{
                log.info("卸载插件：{}---{}", qqPluginVo.getName(), "插件不存在");
            }
        }
    }

    /**
     * 销毁机器人插件jar
     * @param shr25PluginClassLoad
     * @param isDeleteJar 是否删除jar
     */
    private void destroyRobotPluginJar(Shr25PluginClassLoad shr25PluginClassLoad, boolean isDeleteJar){
        for (RobotPlugin robotPlugin : shr25PluginClassLoad.getRobotPlugins()) {
            shr25PluginClassLoadMap.remove(robotPlugin.getClass().getName());
            robotPluginMap.remove(robotPlugin.getClass().getName());
            destroyRobotPlugin(robotPlugin.getClass().getName());
        }
        shr25PluginClassLoad.unLoadJar(isDeleteJar);
    }

    /**
     * 初始化插件缓存
     */
    private void initRobotPlugin() {
        SpringUtil.getBeansOfType(RobotPlugin.class).entrySet().stream().forEach(
            entity -> {
                log.debug("{}====>{}", entity.getKey(), entity.getValue().getClass().getName());
                QqPlugin old = qqPluginService.query().eq("class_name", entity.getValue().getClass().getName()).one();
                if (old == null) {
                    log.debug("自动添加插件到数据库：{}====>{}", entity.getValue().getName(), entity.getValue().getClass().getName());
                    QqPlugin qqPlugin = new QqPlugin();
                    qqPlugin.setName(entity.getValue().getName());
                    qqPlugin.setPluginDesc(entity.getValue().getDesc());
                    qqPlugin.setClassName(entity.getValue().getClass().getName());
                    qqPlugin.setSort(entity.getValue().getSort());
                    qqPlugin.setIsAll(0);
                    qqPlugin.setState(1);
                    qqPlugin.setVersion(entity.getValue().getVersion());
                    qqPluginService.save(qqPlugin);
                }else{
                    if(StringUtils.isNotBlank(entity.getValue().getVersion()) && !entity.getValue().getVersion().equals(old.getVersion())) {
                        old.setVersion(entity.getValue().getVersion());
                        qqPluginService.updateById(old);
                    }
                    entity.getValue().setName(old.getName());
                }
                if(!robotPluginMap.containsKey(entity.getValue().getClass().getName())){
                    robotPluginMap.put(entity.getValue().getClass().getName(), entity.getValue());
                    log.info("开始加载“{}---{}”", entity.getValue().getName(), StringUtils.isBlank(entity.getValue().getVersion())? "插件打包后才可以显示版本" : "V"+entity.getValue().getVersion());
                }
            }
        );

        List<QqPlugin> allPlugins = qqPluginService.query().orderByAsc("sort").list();
        allPlugins = allPlugins.stream().filter(qqPlugin -> {
            try {
                Class clazz = Resources.classForName(qqPlugin.getClassName());
                return true;
            } catch (ClassNotFoundException e) {
                log.warn("未找到插件：" + e.getMessage());
                return false;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return false;
            }
        }).collect(Collectors.toList());

        allPluginVos = allPlugins.stream()
                .map(item -> getQqPluginVo(item))
                .filter(item -> item != null && getRobotPlugin(item).isEnabled())
                .sorted(Comparator.comparing(QqPluginVo::getSort))
                .collect(Collectors.toList());
        publickPlugins = allPluginVos.stream().filter(item -> item.getIsAll() == 1).collect(Collectors.toList());

        qqPluginIdMap.clear();
        qqPluginNameMap.clear();
        allPluginVos.forEach(item -> {
            qqPluginIdMap.put(item.getPluginId(), item);
            qqPluginNameMap.put(item.getName(), item);
        });

        initPlugin();
    }

    /**
     * 销毁插件，同时更新插件缓存
     */
    private void destroyRobotPlugin(String className) {
        allPluginVos = allPluginVos.stream()
                .filter(item -> !item.getClassName().equals(className))
                .sorted(Comparator.comparing(QqPluginVo::getSort))
                .collect(Collectors.toList());

        publickPlugins = allPluginVos.stream().filter(item -> item.getIsAll() == 1).collect(Collectors.toList());

        qqPluginIdMap.clear();
        qqPluginNameMap.clear();
        allPluginVos.forEach(item -> {
            qqPluginIdMap.put(item.getPluginId(), item);
            qqPluginNameMap.put(item.getName(), item);
        });

        initPlugin();
    }

    public void publishMessage(BotEvent event) {
        QqMessage qqMessage = new QqMessage(event, qqConfig, NORMAL_MANAGE_QQ);

        //记录出入群信息
        qqGroupLog(qqMessage);

        if (qqMessage.getRobotMsgType() != null && qqMessage.getRobotMsgType().getMsgType() > 1) {
            if (StringUtils.isNotBlank(qqMessage.getCommand())) {
                switch (qqMessage.getCommand()) {
                    case "help":
                    case "菜单":
                        if (qqMessage.isManager()) {
                            qqMessage.putReplyMessage(getCommandsStr(qqMessage));
                        }
                        break;
                    case "管理":
                        if (qqMessage.isManager()) {
                            qqMessage.putReplyMessage(getDesc());
                        }
                        break;
                    case "初始化":
                        if (qqMessage.isManager()) {
                            initPlugin();
                            qqMessage.putReplyMessage("初始化成功！~~~");
                        }
                        break;
                    case "开机":
                        if (qqMessage.isManager()) {
                            enabled = true;
                            this.robotStatusMessage(qqMessage);
                        }
                        break;
                    case "关机":
                        if (qqMessage.isManager()) {
                            enabled = false;
                            this.robotStatusMessage(qqMessage);
                        }
                        break;
                    case "机器人状态":
                        if (qqMessage.isManager()) {
                            this.robotStatusMessage(qqMessage);
                        }
                        break;
                    case "添加管理员":
                        if (qqMessage.isSuperManager()) {
                            this.addManage(qqMessage, qqMessage.getParameter(), true);
                        }
                        break;
                    case "删除管理员":
                        if (qqMessage.isSuperManager()) {
                            this.addManage(qqMessage, qqMessage.getParameter(), false);
                        }
                        break;
                    case "全部插件":
                    case "所有插件":
                        if (qqMessage.isManager()) {
                            this.allPluginsListMessage(qqMessage);
                        }
                        break;
                    case "插件列表":
                        if (StringUtils.isBlank(qqMessage.getContent())) {
                            this.pluginsListMessage(qqMessage);
                        } else {
                            if (qqMessage.isManager()) {
                                this.pluginsListMessage(qqMessage, qqMessage.getParameter());
                            }
                        }
                        break;
                    case "添加插件":
                        if (qqMessage.isManager()) {
                            this.addPluginsList(qqMessage, qqMessage.getParameter(), true);
                        }
                        break;
                    case "删除插件":
                        if (qqMessage.isManager()) {
                            this.addPluginsList(qqMessage, qqMessage.getParameter(), false);
                        }
                        break;
                    case "开启插件":
                        if (qqMessage.isManager() || qqMessage.isCanOperatorGroup()) {
                            this.startPlugin(qqMessage, qqMessage.getParameter(), true);
                        }
                        break;
                    case "关闭插件":
                        if (qqMessage.isManager() || qqMessage.isCanOperatorGroup()) {
                            this.startPlugin(qqMessage, qqMessage.getParameter(), false);
                        }
                        break;
                    case "加载插件":
                        if (qqMessage.isManager()) {
                            if(StringUtils.isNotBlank(qqMessage.getContent())){
                                String[] args = qqMessage.getContent().split("\\s+");
                                String jaFileName = args[0];
                                this.addRobotPluginJar(jaFileName);
                                this.allPluginsListMessage(qqMessage);
                            }

                        }
                        break;
                    case "切换插件":
                        if (qqMessage.isManager()) {
                            if(StringUtils.isNotBlank(qqMessage.getContent())){
                                String[] args = qqMessage.getContent().split("\\s+");
                                String pluginName = args[0];
                                this.updateRobotPluginJar(pluginName);
                                this.allPluginsListMessage(qqMessage);
                            }
                        }
                        break;
                    case "卸载插件":
                        if (qqMessage.isManager()) {
                            if(StringUtils.isNotBlank(qqMessage.getContent())){
                                String[] args = qqMessage.getContent().split("\\s+");
                                String pluginName = args[0];
                                this.destroyRobotPluginJar(pluginName);
                                this.allPluginsListMessage(qqMessage);
                            }
                        }
                        break;
                    default:
                        //是否插件简介
                        boolean isPluginDesc = false;
                        if (StringUtils.isNotBlank(qqMessage.getCommand())) {
                            QqPluginVo qqPlugin = qqPluginNameMap.get(qqMessage.getCommand());
                            if (qqPlugin != null) {
                                qqMessage.putReplyMessage(getRobotPlugin(qqPlugin).getPluginInfo(qqMessage));
                                isPluginDesc = true;
                            }
                        }
                        if (!isPluginDesc) {
                            execute(qqMessage);
                        }
                        break;
                }
            } else {
                if (qqMessage.getAt() && StringUtils.isBlank(qqMessage.getContent()) && (qqMessage.isManager() || qqMessage.isCanOperatorGroup())) {
                    qqMessage.putReplyMessage(getCommandsStr(qqMessage));
                } else if (!qqMessage.getMessage().isEmpty()) {//分享卡片或普通发言
                    execute(qqMessage);
                }
            }
        } else {
            execute(qqMessage);
        }
        sendMessage(qqMessage);
    }

    /**
     * 获取插件bean
     * @param qqPluginVo
     * @return
     */
    private RobotPlugin getRobotPlugin(QqPluginVo qqPluginVo){
        return robotPluginMap.get(qqPluginVo.getClassName());
    }

    /**
     * 管理功能描述
     * @return
     */
    public String getDesc() {
        return "系统管理 使用方式："
                + "\n初始化"
                + "\n开机"
                + "\n关机"
                + "\n管理员列表"
                + "\n添加管理员 {qq号/@群成员}"
                + "\n删除管理员 {qq号/@群成员}"
                + "\n机器人状态"
                + "\n全部插件/所有插件"
                + "\n插件列表 {群号，管理员发群消息可以省略}"
                + "\n添加插件 {插件名称} {群号，管理员发群消息可以省略}"
                + "\n删除插件 {插件名称} {群号，管理员发群消息可以省略}"
                + "\n开启插件 {插件名称}"
                + "\n关闭插件 {插件名称}"
                + "\n加载插件 {插件文件名称} 如：#加载插件 group-manager-plugin-3.1.0.jar, 可以加载新插件，或重新加载当前插件（插件有问题，调试）"
                + "\n切换插件 {插件名称} 插件目录下有2个不同版本的插件，会卸载并删除当前加载插件，并加载未加载的插件" ;
    }
    /**
     * 记录出入群日志
     *
     * @param qqMessage
     */
    public void qqGroupLog(QqMessage qqMessage) {
        //记录出入群信息
        if (qqMessage.getEvent() instanceof MemberJoinEvent || qqMessage.getEvent() instanceof MemberLeaveEvent) {
            //入群或出群的qq
            Group group = qqMessage.getGroupMemberEvent().getGroup();
            Member member = qqMessage.getGroupMemberEvent().getMember();
            QqGroupLog qqGroupLog = new QqGroupLog();
            qqGroupLog.setGroupId(group.getId());
            qqGroupLog.setQq(member.getId());
            qqGroupLog.setQqName(member.getNick());
            qqGroupLog.setCreateTime(new Date());
            if (qqMessage.getEvent() instanceof MemberJoinEvent.Invite) {
                qqGroupLog.setEvent(1);
                qqGroupLog.setInvite(qqMessage.getInviteMemberJoinEvent().getInvitor().getId());
            } else if (qqMessage.getEvent() instanceof MemberJoinEvent.Active) {
                qqGroupLog.setEvent(2);
            } else if (qqMessage.getEvent() instanceof MemberLeaveEvent.Kick) {
                MemberLeaveEvent.Kick kick = qqMessage.getKickMemberLeaveEvent();
                qqGroupLog.setEvent(3);
                if (kick.getOperator() != null) {
                    qqGroupLog.setKick(kick.getOperator().getId());
                }
            } else if (qqMessage.getEvent() instanceof MemberLeaveEvent.Quit) {
                qqGroupLog.setEvent(4);
            }
            qqGroupLogService.save(qqGroupLog);
        }
    }

    /**
     * 插件处理消息
     * @param qqMessage
     */
    private void execute(QqMessage qqMessage) {
        getPlugins(qqMessage).forEach(item -> {
            if (qqMessage.isExecuteNext() && item.isEnabled()) {
                //插件添加异常捕捉，避免影响，导致程序关闭
                try {
                    getRobotPlugin(item).execute(qqMessage);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        });
    }

    /**
     * 获取所有插件的指令列表
     *
     * @param qqMessage
     * @return
     */
    private String getCommandsStr(QqMessage qqMessage) {
        StringBuilder strMsg = new StringBuilder("我是" + qqConfig.getName() + "！~~~\n指令列表：\n");
        int n = 0;
        if (qqMessage.isManager()) {
            n++;
            strMsg.append(n + "、管理    所有的管理命令\n");
        }
        List<String> allCommands = new ArrayList<>();
        getPlugins(qqMessage).forEach(item -> {
            if (item.isEnabled() && getRobotPlugin(item).isEnabled()) {
                if (qqConfig.isSimplifyCommand()) {
                    allCommands.addAll(getRobotPlugin(item).getAllMasterCommands(qqMessage));
                } else {
                    allCommands.addAll(getRobotPlugin(item).getAllCommands(qqMessage));
                }
            }
        });
        for (String command : allCommands) {
            n++;
            strMsg.append(n + "、" + command + '\n');
        }

        return strMsg.toString();
    }

    /**
     * 获取管理员QQ
     * @return
     */
    private Set<Long> getRootManageQq() {
        return qqConfig.getRootManageQq();
    }

    /**
     * 添加机器人状态消息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void robotStatusMessage(QqMessage qqMessage) {
        qqMessage.putReplyMessage(String.format("机器人状态：%s", enabled));
    }

    /**
     * 添加管理员列表信息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void manageMessage(QqMessage qqMessage) {
        StringBuilder message = new StringBuilder();
        message.append("root管理员列表：\n");
        getRootManageQq().forEach(item -> {
            message.append(item).append("\n");
        });
        message.append("普通管理员列表：\n");
        NORMAL_MANAGE_QQ.forEach(item -> {
            message.append(item).append("\n");
        });
        qqMessage.putReplyMessage(message.toString());
    }

    /**
     * 添加管理员列表
     *
     * @param qqMessage 机器人插件上下文
     * @param qqStr     qq号
     * @param add       是否添加
     */
    private void addManage(QqMessage qqMessage, String qqStr, boolean add) {
        Long qq = getQq(qqStr);
        if (qq != null) {
            if (add) {
                NORMAL_MANAGE_QQ.add(qq);
            } else {
                NORMAL_MANAGE_QQ.remove(qq);
            }

            this.manageMessage(qqMessage);
        } else {
            String error = StrUtil.format("转换qq失败[{}]", qq);
            qqMessage.putReplyMessage(error);
            return;
        }
    }

    /**
     * 转换qq群插件
     *
     * @param qqPlugin
     * @return
     */
    private QqPluginVo getQqPluginVo(QqPlugin qqPlugin) {
        if (qqPlugin != null) {
            QqPluginVo qqPluginVo = new QqPluginVo();
            qqPluginVo.setPluginId(qqPlugin.getId());
            qqPluginVo.setName(qqPlugin.getName());
            qqPluginVo.setPluginDesc(qqPlugin.getPluginDesc());
            qqPluginVo.setIsAll(qqPlugin.getIsAll());
            qqPluginVo.setSort(qqPlugin.getSort());
            qqPluginVo.setPluginEnabled(qqPlugin.getState());
            qqPluginVo.setClassName(qqPlugin.getClassName());
            qqPluginVo.setVersion(qqPlugin.getVersion());
            return qqPluginVo;
        } else {
            return null;
        }
    }

    /**
     * 转换qq群插件
     *
     * @param qqPlugin
     * @return
     */
    private QqPluginVo getQqPluginVo(QqPluginVo qqPlugin) {
        if (qqPlugin != null) {
            QqPluginVo qqPluginVo = new QqPluginVo();
            qqPluginVo.setPluginId(qqPlugin.getPluginId());
            qqPluginVo.setName(qqPlugin.getName());
            qqPluginVo.setPluginDesc(qqPlugin.getPluginDesc());
            qqPluginVo.setIsAll(qqPlugin.getIsAll());
            qqPluginVo.setSort(qqPlugin.getSort());
            qqPluginVo.setEnabled(qqPlugin.isEnabled());
            qqPluginVo.setClassName(qqPlugin.getClassName());
            qqPluginVo.setVersion(qqPlugin.getVersion());
            return qqPluginVo;
        } else {
            return null;
        }
    }

    /**
     * 转换qq群插件
     *
     * @param qqPlugin
     * @param qqGroupPlugin
     * @return
     */
    private QqPluginVo getQqPluginVo(QqPluginVo qqPlugin, QqGroupPlugin qqGroupPlugin) {
        if (qqPlugin != null) {
            QqPluginVo qqPluginVo = getQqPluginVo(qqPlugin);
            qqPluginVo.setId(qqGroupPlugin.getId());
            qqPluginVo.setSort(qqGroupPlugin.getSort());
            qqPluginVo.setPluginEnabled(qqGroupPlugin.getEnabled());
            return qqPluginVo;
        } else {
            return null;
        }
    }

    /**
     * 初始化插件
     */
    private void initPlugin(){
        groupPluginMap.clear();
        List<QqGroupPlugin> groupPlugins = qqGroupPluginService.query().list();
        groupPlugins.forEach(item -> {
            QqPluginVo qqPluginVo = qqPluginIdMap.get(item.getPluginId());

            if(qqPluginVo != null && qqPluginVo.isEnabled()){
                List<QqPluginVo> groupPluginList = groupPluginMap.get(item.getGroupId());
                if (groupPluginList == null) {
                    groupPluginList = new ArrayList<>();
                    groupPluginMap.put(item.getGroupId(), groupPluginList);
                }

                if (qqPluginVo.getIsAll() == 1) {
                    if(item.getEnabled() == 0) { //不启用公共插件
                        qqPluginVo.addNotGroups(item.getGroupId());
                    }
                }else{
                    groupPluginList.add(getQqPluginVo(qqPluginVo, item));
                }
            }
        });
    }

    /**
     * 添加群插件列表
     *
     * @param qqMessage  机器人插件上下文
     * @param pluginName 插件名称
     * @param add        是否添加
     */
    private void addPluginsList(QqMessage qqMessage, String pluginName, boolean add) {
        String[] pluginNames = pluginName.split("\\s+");
        Long groupId = qqMessage.getGroupId();
        QqPluginVo qqPluginVo = null;
        if (pluginNames.length == 1) {
            qqPluginVo = qqPluginNameMap.get(pluginNames[0]);
        } else if (pluginNames.length == 2) {
            groupId = getQq(pluginNames[1]);
            qqPluginVo = qqPluginNameMap.get(pluginNames[0]);
        }
        if (qqPluginVo != null && qqPluginVo.getIsAll() == 0 && groupId != null) {
            Group group = MessageUtil.getGroup(groupId);

            if (group != null) {
                QqGroupPlugin old = qqGroupPluginService.getGroupPlugin(groupId, qqPluginVo.getPluginId());
                if (add) {
                    if (old == null) {
                        qqGroupPluginService.addQqGroupPlugin(groupId, add, qqPluginVo, qqMessage.getSender());
                    }else if(old.getEnabled() == 0){
                        qqGroupPluginService.enabled(old.getId());
                    }
                } else {
                    if (old != null) {
                        qqGroupPluginService.removeById(old.getId());
                    }
                }

                initPlugin();
                //返回插件列表
                this.pluginsListMessage(qqMessage, groupId);

                if (add) {
                    getRobotPlugin(qqPluginVo).init(groupId);
                } else {
                    getRobotPlugin(qqPluginVo).cancel(groupId);
                }
            } else {
                String error = StrUtil.format("未找到QQ群：[{}]", groupId);
                qqMessage.putReplyMessage(error);
            }
        }else if (qqPluginVo != null  && qqPluginVo.getIsAll() == 1){//开启插件关闭总开关
            if (add){
                qqPluginService.enabled(qqPluginVo.getPluginId());
                if(qqPluginVo != null) {
                    qqPluginVo.setEnabled(true);
                }
            } else {
                 qqPluginService.disable(qqPluginVo.getPluginId());
                if(qqPluginVo != null) {
                    qqPluginVo.setEnabled(false);
                }
            }

            initPlugin();
            //返回插件列表
            this.pluginsListMessage(qqMessage, groupId);
        } else if (qqPluginVo == null) {
            String error = StrUtil.format("未找到[{}]", pluginNames[0]);
            qqMessage.putReplyMessage(error);
        } else if (groupId == null) {
            String error = StrUtil.format("未找到qq群");
            qqMessage.putReplyMessage(error);
        }

    }

    /**
     * 根据插件名称启动插件
     *
     * @param qqMessage  机器人插件上下文
     * @param pluginName 插件名称{@link RobotPlugin#getPluginInfo(QqMessage)}
     * @param start      是否启用
     */
    public void startPlugin(QqMessage qqMessage, String pluginName, boolean start) {
        String[] pluginNames = pluginName.split("\\s+");
        String name = pluginNames[0];
        Long groupId = null;
        if (pluginNames.length == 1) {
            groupId = qqMessage.getGroupId();
        }else if (pluginNames.length == 2) {
            groupId = getQq(pluginNames[1]);
        }
        if (groupId != null) {
            Long finalGroupId = groupId;
            getPlugins(groupId).forEach(item -> {
                if (item.getName().equals(name)) {
                    if(item.getIsAll() == 0) {
                        item.setEnabled(start);
                        if(start){
                            log.info("{}--启动插件：{}", finalGroupId, item.getName());
                            qqGroupPluginService.enabled(item.getId());
                        }else{
                            log.info("{}--关闭插件：{}", finalGroupId, item.getName());
                            qqGroupPluginService.disable(item.getId());
                        }
                    }else{
                        QqPluginVo publicPluginVo = qqPluginIdMap.get(item.getPluginId());
                        QqGroupPlugin old = qqGroupPluginService.getGroupPlugin(finalGroupId, item.getPluginId());
                        if(start){
                            log.info("{}--启动公共插件：{}", finalGroupId, item.getName());
                            publicPluginVo.removeNotGroups(finalGroupId);
                            if(old != null && old.getEnabled() == 0){
                                qqGroupPluginService.removeById(old.getId());
                            }
                        }else{
                            log.info("{}--关闭公共插件：{}", finalGroupId, item.getName());
                            publicPluginVo.addNotGroups(finalGroupId);
                            if (old == null) {
                                qqGroupPluginService.addQqGroupPlugin(finalGroupId, start, item, qqMessage.getSender());
                            }else if(old.getEnabled() == 1){
                                qqGroupPluginService.disable(old.getId());
                            }

                        }
                    }

                    if (start) {
                        getRobotPlugin(item).init(qqMessage.getGroupId());
                    } else {
                        getRobotPlugin(item).cancel(qqMessage.getGroupId());
                    }
                }
            });
            this.pluginsListMessage(qqMessage, groupId);
        }else{
            qqMessage.putReplyMessage("缺少参数groupId："+groupId);
        }
    }

    /**
     * 转换qq
     *
     * @param qqStr
     * @return
     */
    private Long getQq(String qqStr) {
        Long qq = null;
        if (!StringUtil.isBlank(qqStr)) {
            try {
                qq = Long.parseLong(qqStr);
            } catch (Exception e) {
                log.error("转换qq号异常：{}", qqStr);
            }
        }
        return qq;
    }

    /**
     * 获取插件列表
     *
     * @param qqMessage qq消息
     */
    private List<QqPluginVo> getPlugins(QqMessage qqMessage) {
        if (qqMessage.getGroupId() != null) {
            return groupPluginSort(qqMessage.getGroupId());
        } else if (qqMessage.isManager()) {
            return allPluginVos;
        } else {
            return publickPlugins;
        }
    }

    /**
     * 获取群插件列表
     *
     * @param groupId qq群Id
     */
    private List<QqPluginVo> getPlugins(Long groupId) {
        return groupPluginSort(groupId);
    }

    /**
     * 添加插件列表消息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void allPluginsListMessage(QqMessage qqMessage) {
        StringBuffer text = new StringBuffer();
        if (qqMessage.isManager()) {
            allPluginVos.forEach(item -> {
                text.append(String.format(template, item.getName(), item.isEnabled()));
            });
        }
        if (text.toString() != "") {
            qqMessage.putReplyMessage(text.toString());
        }
    }
    /**
     * 添加插件列表消息
     */
    public List<QqPluginVo> getAllPluginVos() {
        return allPluginVos;
    }

    /**
     * 添加插件列表消息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void pluginsListMessage(QqMessage qqMessage) {
        pluginsListMessage(qqMessage, qqMessage.getGroupId());
    }

    /**
     * 添加插件列表消息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void pluginsListMessage(QqMessage qqMessage, String groupIdStr) {
        Long groupId = getQq(groupIdStr);

        if (groupId != null) {
            pluginsListMessage(qqMessage, groupId);
        }
    }

    /**
     * 添加插件列表消息
     *
     * @param qqMessage 机器人插件上下文
     */
    private void pluginsListMessage(QqMessage qqMessage, Long groupId) {
        if (groupId != null) {
            StringBuffer text = new StringBuffer();

            getPlugins(groupId).forEach((item) -> {
                text.append(String.format(template, getRobotPlugin(item).getName(), item.isEnabled()));
            });

            if (!text.toString().equals("")) {
                qqMessage.putReplyMessage(text.toString());
            }
        }
    }


    /**
     * 插件排序
     * @param groupId
     * @return
     */
    public List<QqPluginVo> groupPluginSort(Long groupId) {
        //群可以使用的公共插件
        List<QqPluginVo> groupPublickPlugins =  publickPlugins.stream()
                .map(item -> {
                    QqPluginVo publicQqPluginVo = getQqPluginVo(item);
                    publicQqPluginVo.setEnabled(publicQqPluginVo.isEnabled() && !item.getNotGroups().contains(groupId));
                    return publicQqPluginVo;
                })
                .filter(item -> !item.getNotGroups().contains(groupId)).collect(Collectors.toList());
        Set<QqPluginVo> groupPlugins = new TreeSet();
        groupPlugins.addAll(groupPublickPlugins);
        if(groupPluginMap.get(groupId) != null) {
            groupPlugins.addAll(groupPluginMap.get(groupId));
        }
        return groupPlugins.stream().sorted(Comparator.comparing(QqPluginVo::getSort)).collect(Collectors.toList());
    }

    /**
     * 从上下文中获取需要发送的消息
     *
     * @param qqMessage 机器人上下文
     */
    private void sendMessage(QqMessage qqMessage) {
        if (!qqMessage.getReplyMessages().isEmpty()) {
            qqMessage.getReplyMessages().entries().forEach(entry -> {
                entry.getKey().sendMessage(entry.getValue());
            });
        }
    }

    /**
     * 获取版本号
     * @return
     */
    public String getVersion(){
        if(org.apache.commons.lang3.StringUtils.isBlank(version)){
            Manifest manifest = ManifestUtils.getJarManifest(getClass());
            if(manifest!= null) {
                Attributes attributes = manifest.getMainAttributes();
                version = attributes.getValue("Shr25-Robot-Plugin-Version");
            }
        }
        return version;
    }
}
