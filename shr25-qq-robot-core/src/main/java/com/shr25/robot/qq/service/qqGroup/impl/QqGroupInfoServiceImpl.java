package com.shr25.robot.qq.service.qqGroup.impl;

import com.shr25.robot.base.MyBaseServiceImpl;
import com.shr25.robot.qq.mapper.qqGroup.QqGroupInfoMapper;
import com.shr25.robot.qq.model.QqGroupInfo;
import com.shr25.robot.qq.service.qqGroup.IQqGroupInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * QQ群信息Service业务层处理
 *
 * @author huobing
 * @date 2022-6-20 16:37
 */
@Slf4j
@Service
public class QqGroupInfoServiceImpl extends MyBaseServiceImpl<QqGroupInfoMapper, QqGroupInfo> implements IQqGroupInfoService {

    @Override
    public QqGroupInfo getByGroupId(Long groupId) {
        return lambdaQuery().eq(QqGroupInfo::getGroupId, groupId).one();
    }

    @Override
    public List<QqGroupInfo> findByKeyword(String keyword) {
        return lambdaQuery().like(QqGroupInfo::getKeyword, ","+keyword+",").list();
    }
}
