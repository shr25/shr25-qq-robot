package com.shr25.robot.qq.service.qqGroup;

import com.shr25.robot.base.MyBaseService;
import com.shr25.robot.qq.model.QqGroupPlugin;
import com.shr25.robot.qq.model.Vo.QqPluginVo;
import net.mamoe.mirai.contact.User;

/**
 * QQ群插件Service接口
 *
 * @author huobing
 * @date 2022-06-14 14:13:04
*/
public interface IQqGroupPluginService extends MyBaseService<QqGroupPlugin> {

    /**
     * 获取群插件信息
     * @param groupId
     * @param pluginid
     * @return
     */
    QqGroupPlugin getGroupPlugin(Long groupId, Long pluginid);

    /**
     * 开启插件
     * @param id
     */
    void enabled(Long id);

    /**
     * 关闭插件
     * @param id
     */
    void disable(Long id);

    /**
     * 添加群插件信息
     * @param groupId
     * @param enabled
     * @param qqPluginVo
     * @param sender
     */
    QqGroupPlugin addQqGroupPlugin(Long groupId, Boolean enabled, QqPluginVo qqPluginVo, User sender);
}
